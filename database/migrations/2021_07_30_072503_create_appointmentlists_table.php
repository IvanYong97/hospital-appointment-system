<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_lists', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('patientID')->references('patientID')->on('patients');
            $table->date('appointmentDate');
            $table->string('status');
            $table->foreignId('doctorID')->nullable()->references('doctorID')->on('doctors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointmentlists');
    }
}

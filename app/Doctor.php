<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model 
{
    protected $fillable = ['name', 'userId', 'departmentId'];

    public function Appointments(){
        return $this->hasMany('App\AppointmentList', 'doctorID', 'doctorID');
    }

    public function departments(){
        return $this->belongsTo('App\SpecialListDepartments', 'departmentID', 'departmentID');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentList extends Model
{
    public function patients(){
        return $this->belongsTo('App\Patient', 'patientID', 'patientID');
    }

    public function doctors(){
        return $this->belongsTo('App\Doctor', 'doctorID', 'doctorID');
    }
}

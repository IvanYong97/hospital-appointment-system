<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = ['name', 'mobile', 'appointment_status'];

    public function Appointments(){
        return $this->hasMany('App\AppointmentList', 'patientID', 'patientID');
    }
}

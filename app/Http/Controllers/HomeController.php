<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Doctor;
use App\Nurse;
use App\Patient;
use App\AppointmentList;
use App\SpecialListDepartment;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userType = Auth::user()->userType;
        $lists = null;
        if ($userType == 'nurse') {
            $lists = AppointmentList::with('patients')->get();
        } elseif ($userType == 'doctor') {
            $id =  Auth::user()->id;
            $doctor = Doctor::where('userId', $id)->firstOrFail();
            $lists = AppointmentList::with('patients')->get()->where('doctorID', "=", $doctor->doctorID);
        }
        return view('pages.home', ['lists' => $lists, 'userType' => $userType]);
    }
    public function search(Request $request)
    {
        $userType = Auth::user()->userType;
        $lists = null;
        if ($userType == 'nurse') {
            $lists = AppointmentList::with('patients')->where('appointmentCode', $request->search)->get();
        } elseif ($userType == 'doctor') {
            $id =  Auth::user()->id;
            $doctor = Doctor::where('userId', $id)->firstOrFail();
            $lists = AppointmentList::with('patients')->get()
                ->where('doctorID', "=", $doctor->doctorID)
                ->where('appointmentCode', $request->search);
        }
        return view('pages.home', ['lists' => $lists, 'userType' => $userType]);
    }

    public function createDepartment()
    {
        return view('pages.createDepartment');
    }
    public function setDepartment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
        ]);
        if ($validator->fails()) {
            return redirect('/appointment')
                ->withErrors($validator)
                ->withInput();
        }
        $department = new SpecialListDepartment();
        $department->departmentName = $request->name;
        $department->save();
        return redirect('/home');
    }

    public function assignDepartmentView()
    {
        $id = Auth::user()->id;
        $doctor = Doctor::where('userId', $id)->first();
        $department = SpecialListDepartment::pluck('departmentName', 'departmentID');
        return view('pages.assignDepartment', ['doctor'=>$doctor, 'department'=>$department]);
    }
    public function assignDepartment(Request $request)
    {
        
        $doctor = Doctor::where('doctorID', $request->id)->update(['departmentID' => $request->department]);
        return redirect('/home');
    }
}

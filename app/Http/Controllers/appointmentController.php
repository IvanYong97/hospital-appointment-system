<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\AppointmentList;
use App\Doctor;
use App\Mail\TestMail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class appointmentController extends Controller
{
    function index()
    {
        return view('pages.MakeAppointment');
    }

    function createAppointment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'mobile' => 'required|numeric',
            'ic' => [
                'required',
                'string', 
                'min:10', 
                'max:12',
                'regex:/^[A-Za-z0-9 ]+$/'
            ],
            'email' => [
                'required',
                'regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix'
            ],
            'appointmentDate' => 'required',
            'symptom'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('/appointment')
                ->withErrors($validator)
                ->withInput();
        }
        $patient = new Patient();
        $patient->name = $request->name;
        $patient->IC = $request->ic;
        $patient->appointment_status = "pending";
        $patient->email = $request->email;
        $patient->mobile = $request->mobile;
        $patient->save();
        $appointment = new AppointmentList();
        $time = strtotime($request->appointmentDate);
        $newformat = date('Y-m-d', $time);
        $appointment->appointmentDate = $newformat;
        $appointment->status = "pending";
        $appointment->symptom = $request->symptom;
        $appointment->patientID = $patient->id;
        $random = Str::random(20);
        $appointment->appointmentCode = $random;
        $appointment->save();
        return redirect('/');
    }

    public function getDetail($id)
    {
        $list = AppointmentList::with('patients')->where('id', '=', $id)->get();
        $doctors = Doctor::pluck('name', 'doctorID');
        return view('pages.details', ['lists' => $list, 'doctors' => $doctors]);
    }

    public function updateAppointment(Request $request)
    {
        $list = AppointmentList::with('patients')->where('id', '=', $request->id)
            ->update(['doctorID' => $request->doctor]);
        if ($list) {
            $appointment = AppointmentList::with('patients', 'doctors')->where('id', '=', $request->id)->first();
            $data = [
                "name" => $appointment->patients->name,
                "appointmentCode" => $appointment->appointmentCode,
                "appointmentDate" => $appointment->appointmentDate,
                "doctor" => $appointment->doctors->name
            ];
            Mail::to($appointment->patients->email)->send(new TestMail($data));
        }
        return redirect("/home");
    }
    public function getDetailDoc($id)
    {
        $list = AppointmentList::with('patients')->where('id', '=', $id)->get();
        return view('pages.detailsDoc', ['lists' => $list]);
    }

    public function updatePatientStatus(Request $request)
    {

        $patient = Patient::where("patientID", $request->id)->update(['appointment_status' => $request->status]);
        return redirect('/home');
    }
    public function getConfirmDetail($id)
    {
        $list = AppointmentList::with('patients', 'doctors')->where('id', '=', $id)->get();

        return view('pages.confirmAppointment', ['lists' => $list]);
    }
    public function setConfirmDetail(Request $request)
    {
        $list = AppointmentList::where('id', $request->id)->update(['status' => 'done']);

        return redirect('/home');
    }
}

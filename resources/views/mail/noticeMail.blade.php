<div class="container">
    <div class="row justify-content-center">
        <p>Hello {{ $mail_data['name'] }},</p>
        <p>Your appointment have being confirmed.</p>
        <p>Your appointment code is {{ $mail_data['appointmentCode'] }}</p>
        <p>Your Doctor is {{ $mail_data['doctor'] }}</p>
        <p>Appointment Date is {{ $mail_data['appointmentDate'] }}</p>
        <p>Thank you.</p>
        <p>Laravel Hospital App</p>
    </div>
</div> 






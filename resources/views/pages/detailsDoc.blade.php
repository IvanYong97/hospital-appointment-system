@extends('layouts.app')
@section('head')
<link href="{{ asset('css/details.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container">
    <div id="details">
        <div class="row no-gutters justify-content-center">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-hearder">
                        <h3 class="form-title ">Appointment Details</h3>
                    </div>
                    <div class="card-body">
                        @foreach($lists as $list)

                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Appointment Code: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{{ $list->appointmentCode }}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Appointment Date: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{!! $list->appointmentDate !!}</p>
                            </div>
                        </div>

                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Patient Name: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{{ $list->patients->name }}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>IC Number: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{!! $list->patients->IC !!}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Email: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{!! $list->patients->email !!}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Phone Number: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{!! $list->patients->mobile !!}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Symptom: </p>
                            </div>
                            <div class="col-xl-5">
                                <p>{!! $list->symptom !!}</p>
                            </div>
                        </div>
                        @endforeach
                        {{ Form::open(array('route' => 'docComfirm.update', 'method' => 'PUT')) }}
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                <p>Patient Status: </p>
                            </div>
                            <div class="col-xl-5">
                                {{ Form::hidden('id', $list->patients->patientID)}}
                                {{ Form::select('status', ['pending'=> 'Pending', 'done'=>"Done"],$list->patients->appointment_status, ['class'=>'contentInput'])}}
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-4">
                                {{ Form::submit('Submit', array('class'=>'btn-submit'))}}
                            </div>
                            {{ Form::close()}}
                            <div class="col-xl-5">
                                <a href="{{ route('home') }}"><button class="btn-back">Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
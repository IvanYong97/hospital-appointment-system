<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hospital Appointment System</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/makeAppointment.css')}}" rel="stylesheet" />

    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="makeAppointment">
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-hearder">
                            <h3 class="form-title ">Appointment Form</h3>
                        </div>
                        <div class="card-body">
                            {{ Form::open(array('route'=> 'appointment.create'))}}
                            <div class="row">
                                <div class="col-md">
                                    {{ Form::label('name', "*Name: ")}}
                                    {{ Form::text('name', null, array('placeholder'=>'Example: Name as IC', 'class'=>'contentInput')) }}
                                    @if ($errors->has('name'))
                                    <p class="error">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="col-md">
                                    {{ Form::label('mobile', '*Mobile Number:')}}
                                    {{ Form::text('mobile', null, ['placeholder'=>'Example: 0161234567','class'=>'contentInput'])}}
                                    @if ($errors->has('mobile'))
                                    <p class="error">{{ $errors->first('mobile') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md">
                                    {{ Form::label('ic', '*IC Number: (without "-")')}}
                                    {{ Form::text('ic', null, ['placeholder'=>'Example: 123456121234','class'=>'contentInput'])}}
                                    @if ($errors->has('ic'))
                                    <p class="error">{{ $errors->first('ic') }}</p>
                                    @endif
                                </div>
                                <div class="col-md">
                                    {{ Form::label('email', "*Email Address: ")}}
                                    {{ Form::email('email', null, array('placeholder'=>'Example: abc@gmail.com','class'=>'contentInput')) }}
                                    @if ($errors->has('email'))
                                    <p class="error">{{ $errors->first('email') }}</p>
                                    @endif

                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md">
                                    {{ Form::label('appointmentDate', "*Appointment Date: ")}}
                                    {{ Form::text('appointmentDate', null, array('placeholder'=>'Example: 1/25/2021', 'id'=>'datepicker')) }}
                                    @if ($errors->has('appointmentDate'))
                                    <p class="error">{{ $errors->first('appointmentDate') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md">
                                    {{ Form::label('symptom', "*Symptom: ")}}
                                    {{ Form::textarea('symptom', null, array('class'=>'contentInput')) }}
                                    @if ($errors->has('symptom'))
                                    <p class="error">{{ $errors->first('symptom') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="row pt-3 ">
                                <div class="col-md-10"></div>

                                <div class="col-md-2 d-flex justify-content-end">
                                    {{ Form::submit('Submit', array('class'=>'btn-submit'))}}
                                </div>
                                {{ Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

</body>

</html>
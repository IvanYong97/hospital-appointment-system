@extends('layouts.app')
@section('head')
<link href="{{ asset('css/makeAppointment.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div class="container">
    <div id="makeAppointment">
        <div class="row no-gutters justify-content-center">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-hearder">
                        <h3 class="form-title ">Appointment Details</h3>
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route'=> 'department.update', 'method' => 'PUT'))}}
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-3">
                                <p>Doctor Name: </p>
                            </div>
                            <div class="col-xl-6">
                                <p>{{ $doctor->name }}</p>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col-xl-3"></div>
                            <div class="col-xl-3">
                                <p>Patient Status: </p>
                            </div>
                            <div class="col-xl-5">
                                {{ Form::hidden('id',  $doctor->doctorID)}}
                                {{ Form::select('department', $department, null, ['class'=>'contentInput'])}}
                            </div>
                        </div>
                        <div class="row pt-3 ">
                            <div class="col-md-4"></div>
                            <div class="col-md-4 ">
                                {{ Form::submit('Submit', array('class'=>'btn-submit'))}}
                            </div>

                        </div>
                        {{ Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
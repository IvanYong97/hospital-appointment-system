@extends('layouts.app')
@section('head')
<link href="{{ asset('css/home.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
@endsection
@section('content')
<div class="container">
<div class="row justify-content-end">
        <div class="col-md-4 col-md-offset-3">
            <form action="{{ route('search') }}" method="POST" class="search-form">
                @csrf
                <div class="form-group has-feedback">
            		<label for="search" class="sr-only">Search</label>
            		<input type="text" class="form-control" name="search" id="search" placeholder="Search">
                    <i class="fas fa-search form-control-feedback"></i>
            	</div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Appointment Code</th>
                        <th scope="col">Patient Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lists as $list)
                    <tr>
                        <th scope="row">{{ $list->id }}</th>
                        <td>{{ $list->appointmentCode}}</td>
                        <td>{{ $list->patients->name }}</td>
                        @if($userType == 'nurse')
                        <td>{{ $list->status}}</td>
                        @if($list->status != 'done')
                        <td><a href="{{ route('appointment.details', $list->id) }}"><button class="btn-submit full">Update</button></a></td>
                        @if($list->doctorID != null)
                        <td><a href="{{ route('confirm.details', $list->id) }}"><button class="btn-submit full">Confirm</button></a></td>
                        @endif
                        @endif
                        @elseif($userType == 'doctor')
                        <td>{{ $list->patients->appointment_status}}</td>
                        <td><a href="{{ route('docComfirm.details', $list->id) }}"><button class="btn-submit">Update</button></a></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
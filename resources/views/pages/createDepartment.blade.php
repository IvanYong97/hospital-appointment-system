@extends('layouts.app')
@section('head')
<link href="{{ asset('css/makeAppointment.css')}}" rel="stylesheet" />
@endsection
@section('content')
<div class="container">
    <div id="makeAppointment">
        <div class="row no-gutters justify-content-center">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-hearder">
                        <h3 class="form-title ">Create Department</h3>
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route'=> 'department.create'))}}
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::label('name', "*Department Name: ")}}
                                {{ Form::text('name', null, array('placeholder'=>'Example:Special Department', 'class'=>'contentInput')) }}
                                @if ($errors->has('name'))
                                <p class="error">{{ $errors->first('name') }}</p>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                {{ Form::submit('Submit', array('class'=>'btn-submit'))}}
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        {{ Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
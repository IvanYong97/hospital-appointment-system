<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Hospital Appointment System</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet" />
    <!-- Styles -->
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="home-title m-b-md">
                Hospital Appointment System
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row no-gutters">
                        <div class="col-xl-4"></div>
                        <div class="col-xl-4">
                            <a href="{{ route('login') }}"><button class="btn btn-primary">Login</button></a>
                            <a href="{{ route('appointment.get') }}"><button class="btn btn-primary">Make a appointment</button></a>
                        </div>
                        <div class="col-xl-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
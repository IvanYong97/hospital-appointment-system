<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\appointmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/search', 'HomeController@search')->name('search');

Route::group(['middleware' => 'auth', 'prefix' => 'department', 'as' => 'department.'], function () {
    Route::get('/create', ['as' => 'get', 'uses' => 'HomeController@createDepartment']);
    Route::post('/create', ['as' => 'create', 'uses' => 'HomeController@setDepartment']);
    Route::get('/assign',  ['as' => 'assign', 'uses' => 'HomeController@assignDepartmentView']);
    Route::put('/assign', ['as' => 'update', 'uses' => 'HomeController@assignDepartment']);
});

Route::group(['prefix' => 'appointment', 'as' => 'appointment.'], function () {
    Route::get('/', ['as' => 'get', 'uses' => 'appointmentController@index']);
    Route::post('/', ['as' => 'create', 'uses' => 'appointmentController@createAppointment']);
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/{id}', ['as' => 'details', 'uses' => 'appointmentController@getDetail']);
        Route::put('/', ['as' => 'update', 'uses' => 'appointmentController@updateAppointment']);
    });
});

Route::group(['middleware' => 'auth', 'prefix' => 'confirmAppointment', 'as' => 'confirm.'], function () {
    Route::get('/{id}', ['as' => 'details', 'uses' => 'appointmentController@getConfirmDetail']);
    Route::put('/', ['as' => 'update', 'uses' => 'appointmentController@setConfirmDetail']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'appointmentDoc', 'as' => 'docComfirm.'], function () {
    Route::get('/{id}', ['as' => 'details', 'uses' => 'appointmentController@getDetailDoc']);
    Route::put('/', ['as' => 'update', 'uses' => 'appointmentController@updatePatientStatus']);
});
